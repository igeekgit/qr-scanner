//
//  MenuVC.swift
//  Qr Scanner
//
//  Created by Winsant on 04/07/19.
//  Copyright © 2019 Winsant. All rights reserved.
//

import UIKit
import GoogleMobileAds

class MenuVC: BaseVC, GADInterstitialDelegate {
    @IBOutlet weak var btnShare: UIButton!
    @IBOutlet weak var btnRate: UIButton!
    @IBOutlet weak var btnMore: UIButton!
    @IBOutlet weak var btnAbout: UIButton!
    
    var interstitial: GADInterstitial!

    
    @IBOutlet weak var bannerView: GADBannerView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setNavigationBar(title: GlobalConstants.APPNAME as NSString, titleImage: nil, leftImage: nil, rightImage: nil, leftTitle: nil, rightTitle: nil, isLeft: false, isRight: false, isLeftMenu: false, isRightMenu: false, bgColor: UIColor.AppColor(), textColor: UIColor.TextColor(), leftClick: { (sender) in
            
        }) { (sender) in
            
        }
        
        self.loadBannerAds(banner: bannerView)
        self.loadInterstitialAd()
        
        self.btnShare.layer.cornerRadius = 5.0
        self.btnRate.layer.cornerRadius = 5.0
        self.btnMore.layer.cornerRadius = 5.0
        self.btnAbout.layer.cornerRadius = 5.0
        
        
    }
    
    func loadInterstitialAd(){
        interstitial = GADInterstitial(adUnitID: "ca-app-pub-5878356407579198/2467578253")
        let request = GADRequest()
        interstitial.load(request)
        interstitial.delegate = self
    }
    
    func interstitialDidReceiveAd(_ ad: GADInterstitial) {
        print("interstitialDidReceiveAd")
        if interstitial.isReady {
            interstitial.present(fromRootViewController: self)
        }
    }
    
    /// Tells the delegate an ad request failed.
    func interstitial(_ ad: GADInterstitial, didFailToReceiveAdWithError error: GADRequestError) {
        print("interstitial:didFailToReceiveAdWithError: \(error.localizedDescription)")
    }
    
    /// Tells the delegate that an interstitial will be presented.
    func interstitialWillPresentScreen(_ ad: GADInterstitial) {
        print("interstitialWillPresentScreen")
    }
    
    /// Tells the delegate the interstitial is to be animated off the screen.
    func interstitialWillDismissScreen(_ ad: GADInterstitial) {
        print("interstitialWillDismissScreen")
    }
    
    /// Tells the delegate the interstitial had been animated off the screen.
    func interstitialDidDismissScreen(_ ad: GADInterstitial) {
        print("interstitialDidDismissScreen")
    }
    
    /// Tells the delegate that a user click will open another app
    /// (such as the App Store), backgrounding the current app.
    func interstitialWillLeaveApplication(_ ad: GADInterstitial) {
        print("interstitialWillLeaveApplication")
    }
    
    @IBAction func btnShareApp(_ sender: Any) {
        self.shareApp()
    }
    
    @IBAction func btnRateApp(_ sender: Any) {
        self.openAppPage()
    }
    
    @IBAction func btnMoreAppClick(_ sender: Any) {
        guard let url = URL(string: "https://apps.apple.com/us/developer/yash-world-pvt-ltd/id1382221443") else {
            return
        }
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url)
        }
    }
    
    @IBAction func btnAboutClick(_ sender: Any) {
        let about = self.storyboard?.instantiateViewController(withIdentifier: "AboutVC") as! AboutVC
        about.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(about, animated: true)
    }
}
