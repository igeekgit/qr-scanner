//
//  AboutVC.swift
//  Qr Scanner
//
//  Created by Winsant on 04/07/19.
//  Copyright © 2019 Winsant. All rights reserved.
//

import UIKit

class AboutVC: BaseVC {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let leftImg : UIImage = UIImage(named: "icoBack")!
        self.setNavigationBar(title: GlobalConstants.APPNAME as NSString, titleImage: nil, leftImage: leftImg, rightImage: nil, leftTitle: nil, rightTitle: nil, isLeft: true, isRight: false, isLeftMenu: false, isRightMenu: false, bgColor: UIColor.AppColor(), textColor: UIColor.TextColor(), leftClick: { (sender) in
            self.navigationController?.popViewController(animated: true)
        }){(sender) in
            
        }
    }
}
