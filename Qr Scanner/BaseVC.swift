//
//  BaseVC.swift
//  Denny
//
//  Created by Naaz Infotech on 11/26/16.
//  Copyright © Naaz Infotech . All rights reserved.
//

import Foundation
import UIKit
import CoreData
import UserNotifications
import UserNotificationsUI
import MessageUI
import QuartzCore
import CoreLocation
import EventKit
import SQLite3
import GoogleMobileAds



//swiftlint:disable all
typealias LeftButton = (_ left: UIButton) -> Void
typealias RightButton = (_ right: UIButton) -> Void

class ClosureSleeve {
    let closure: () -> ()
    
    init(attachTo: AnyObject, closure: @escaping () -> ()) {
        self.closure = closure
        objc_setAssociatedObject(attachTo, "[\(arc4random())]", self, .OBJC_ASSOCIATION_RETAIN)
    }
    @objc func invoke() {
        closure()
    }
}

class BaseVC: UIViewController,MFMailComposeViewControllerDelegate, GADBannerViewDelegate {
    let locationManager = CLLocationManager()
    var imgEmptyDataSet = UIImage()
    var titleEmptyDataSet = String()
    var currentLatitude = String()
    var currentLongitude = String()
    var db: OpaquePointer?
    var arrThoughtsData = NSArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.createDB()
        
        
    }
    
    func createDB() {
        let fileURL = try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
                .appendingPathComponent("QrCode.sqlite")
            
        print("Database path :",fileURL)
        if sqlite3_open(fileURL.path, &db) != SQLITE_OK {
            print("error opening database")
        }
        
        if sqlite3_exec(db, "CREATE TABLE IF NOT EXISTS qrData (id INTEGER PRIMARY KEY AUTOINCREMENT, qrCode TEXT, title TEXT, favorite INTEGER, date TEXT)", nil, nil, nil) != SQLITE_OK {
            let errmsg = String(cString: sqlite3_errmsg(db)!)
            print("error creating table: \(errmsg)")
        }
        
    }
    
    
    func getCurrentDate() -> String {
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "dd.MM.yyyy"
        let result = formatter.string(from: date)
        return result
    }
    
    
    func insertData(qrCode : NSString, title : NSString) {
        var stmt: OpaquePointer?
        
        let strData = self.getCurrentDate()
        
        let queryString = "INSERT INTO qrData (id, qrCode, title, favorite, date) VALUES (?,?,?,?,?)"
        
        if sqlite3_prepare(db, queryString, -1, &stmt, nil) != SQLITE_OK{
            let errmsg = String(cString: sqlite3_errmsg(db)!)
            print("error preparing insert: \(errmsg)")
            return
        }
        
        let strQuote = qrCode
        if sqlite3_bind_text(stmt, 2, strQuote.utf8String , -1, nil) != SQLITE_OK{
            let errmsg = String(cString: sqlite3_errmsg(db)!)
            print("failure binding title: \(errmsg)")
            return
        }
        
        let strTitle = title
        if sqlite3_bind_text(stmt, 3, strTitle.utf8String , -1, nil) != SQLITE_OK{
            let errmsg = String(cString: sqlite3_errmsg(db)!)
            print("failure binding title: \(errmsg)")
            return
        }
        
        let fav: Int = 0
        if sqlite3_bind_int(stmt, 4, Int32(fav)) != SQLITE_OK{
            let errmsg = String(cString: sqlite3_errmsg(db)!)
            print("failure binding title: \(errmsg)")
            return
        }
        
        let date: NSString = strData as NSString
        if sqlite3_bind_text(stmt, 5, date.utf8String , -1, nil) != SQLITE_OK
        {
            let errmsg = String(cString: sqlite3_errmsg(db)!)
            print("failure binding title: \(errmsg)")
            return
        }
        
        if sqlite3_step(stmt) == SQLITE_DONE {
            print("Successfully inserted row.")
            
        } else {
            print("Could not insert row.")
        }
        
    }
    
    
    func loadBannerAds(banner : GADBannerView)  {
        banner.adUnitID = GlobalConstants.adBannerId
        banner.rootViewController = self
        banner.load(GADRequest())
        banner.delegate = self
    }
    
//    func loadIntertitialAds(interstitial: GADInterstitial) {
//        interstitial = GADInterstitial(adUnitID: "ca-app-pub-3940256099942544/4411468910")
//        let request = GADRequest()
//        interstitial.load(request)
//        
//        if interstitial.isReady {
//            interstitial.present(fromRootViewController: self)
//        }
//
//    }
    
    
    func setButton(sender : UIButton) {
        sender.layer.borderColor = UIColor.black.cgColor
        sender.layer.cornerRadius = 5.0
        sender.layer.borderWidth = 1.0
    }
    
    func shareContent(strContent: String) {
        let shareAll = [strContent]
        let activityViewController = UIActivityViewController(activityItems: shareAll, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view
        self.present(activityViewController, animated: true, completion: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func showAlert(title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: { (_) in
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    func drawLIne(label : UILabel)
    {
        let labelLine = UILabel(frame: CGRect(x: 0 , y: label.frame.height / 2, width: label.frame.width, height: 1))
        labelLine.backgroundColor = UIColor.darkGray
        label.addSubview(labelLine)
    }
    
    // MARK: Navigation
    func setNavigationBar(title: NSString? ,
                        titleImage: UIImage?,
                        leftImage: UIImage? ,
                        rightImage: UIImage?,
                        leftTitle: String?,
                        rightTitle: String?,
                        isLeft: Bool ,
                        isRight: Bool,
                        isLeftMenu: Bool ,
                        isRightMenu: Bool ,
                        bgColor: UIColor ,
                        textColor: UIColor,
                        leftClick: @escaping LeftButton ,
                        rightClick: @escaping RightButton)  {
        
        self.navigationItem.hidesBackButton = true

        // Left Item
        let btnLeft: UIButton = UIButton(type: .custom)
        btnLeft.frame = CGRect(x: 0, y: 0, width: 20, height: 20)
        btnLeft.imageView?.contentMode = .scaleAspectFit
        let addImg = leftImage
        if leftTitle != nil {
            btnLeft.setTitle(leftTitle, for: .normal)
            btnLeft.setTitleColor(UIColor.white, for: .normal)
            self.addConstaintsWithWidth(width: 70, height: 30, btn: btnLeft)
        } else {
            btnLeft.setImage(addImg, for: .normal)
            self.addConstaintsWithWidth(width: 25, height: 25, btn: btnLeft)
        }
        btnLeft.sendActions(for: .touchUpInside)
        let leftBarItem: UIBarButtonItem = UIBarButtonItem(customView: btnLeft)
        if isLeft {
            self.navigationItem.leftBarButtonItem = leftBarItem
        }
        if isLeftMenu {
            btnLeft.addTarget(self, action: #selector(btnLeftMenuOpen(sender:)), for: .touchUpInside)
        } else {
            btnLeft.addAction {
                leftClick(btnLeft)
            }
        }
        // Right Item
        let btnRight: UIButton = UIButton(type: .custom)
        btnRight.frame = CGRect(x: self.view.frame.size.width, y: 0, width: 25, height: 25)
        btnRight.imageView?.contentMode = .scaleAspectFit
        let addImg1 = rightImage
        
        if rightTitle != nil {
            self.addConstaintsWithWidth(width: 70, height: 30, btn: btnRight)
            btnRight.titleLabel?.font = UIFont.systemFont(ofSize: 18.0)
            btnRight.titleLabel?.sizeToFit()
            btnRight.setTitleColor(UIColor.white, for: .normal)
            btnRight.setTitle(rightTitle, for: .normal)
        } else {
            self.addConstaintsWithWidth(width: 25, height: 25, btn: btnRight)
            btnRight.setImage(addImg1, for: .normal)
        }
        btnRight.sendActions(for: .touchUpInside)
        let rightBarItem: UIBarButtonItem = UIBarButtonItem(customView: btnRight)
        if isRight {
            self.navigationItem.rightBarButtonItem = rightBarItem
        }
        if isRightMenu {
            btnRight.addTarget(self, action: #selector(btnRightMenuOpen(sender:)), for: .touchUpInside)
        } else {
            btnRight.addAction {
                rightClick(btnRight)
            }
        }
        // title
        if title == nil {
            let xRect: CGFloat = self.view.frame.size.width/2 - 50
            let yRect: CGFloat = self.view.frame.size.height/2 - 40
            let rectImgView: CGRect = CGRect(x: xRect, y: yRect, width: 100, height: 40.0)
            let imgViewTitle = UIImageView(image: titleImage)
            imgViewTitle.backgroundColor = UIColor.clear
            imgViewTitle.contentMode = .scaleAspectFit
            imgViewTitle.frame = rectImgView
            self.navigationItem.titleView = imgViewTitle
        } else {
            let xRect: CGFloat = 0
            let yRect: CGFloat = 0
            let widthRect: CGFloat = self.view.frame.size.width
            let heightRect: CGFloat = 40
            let rectLabel: CGRect = CGRect(x: xRect, y: yRect, width: widthRect, height: heightRect)
            
            let  lblNavigationTitleLabel = UILabel(frame: rectLabel) as UILabel
            lblNavigationTitleLabel.text = title! as String
            lblNavigationTitleLabel.font = UIFont.boldSystemFont(ofSize: 18.0)
            lblNavigationTitleLabel.textColor = textColor
            lblNavigationTitleLabel.textAlignment = .center
            lblNavigationTitleLabel.frame = CGRect(x: 100, y: 0, width: 100, height: 100)
            self.navigationItem.titleView = lblNavigationTitleLabel
        }
        
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.barTintColor = bgColor
        
//        self.addCurvedNavigationBar(backgroundColor: bgColor, curveRadius: -10.0, shadowColor: UIColor.clear, shadowRadius: 2, heightOffset: 0) //swiftLint: disable control_statement
    }
    
    func animateTable(tblView : UITableView) {
        tblView.reloadData()
        
        let cells = tblView.visibleCells
        let tableHeight: CGFloat = tblView.bounds.size.height
        
        for i in cells {
            let cell: UITableViewCell = i as UITableViewCell
            cell.transform = CGAffineTransform(translationX: 0, y: tableHeight)
        }
        
        var index = 0
        
        for a in cells {
            let cell: UITableViewCell = a as UITableViewCell
            
            UIView.animate(withDuration: 1, delay: 0.05 * Double(index), usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: [], animations: {
                cell.transform = CGAffineTransform(translationX: 0, y: 0)
            }, completion: nil)
            index += 1
        }
    }
    
    @objc func btnLeftMenuOpen(sender: UIButton) {
//        panel?.openLeft(animated: true)
    }

    @objc func btnRightMenuOpen(sender: UIButton) {

    }

    func addConstaintsWithWidth(width: CGFloat ,height: CGFloat, btn: UIButton) {
        NSLayoutConstraint(item: btn, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: width).isActive = true
        NSLayoutConstraint(item: btn, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: height).isActive = true
    }
    
    func addEventToCalendar(title: String, description: String?, startDate: Date, endDate: Date,location: EKStructuredLocation, completion: ((_ success: Bool, _ error: NSError?) -> Void)? = nil) {
        let eventStore = EKEventStore()
        let dayBefore = startDate.addingTimeInterval(-1*24*60*60)
        let alermOneDayBefore = EKAlarm(absoluteDate: dayBefore)
        
        let hourBefore = startDate.addingTimeInterval(-1*60*60)
        let alermOneHourBefore = EKAlarm(absoluteDate: hourBefore)
        eventStore.requestAccess(to: .event, completion: { (granted, error) in
            if (granted) && (error == nil) {
                let event = EKEvent(eventStore: eventStore)
                event.title = title
                event.startDate = startDate
                event.endDate = endDate
                event.notes = description
                event.structuredLocation = location
                event.alarms = [alermOneDayBefore, alermOneHourBefore]
                event.calendar = eventStore.defaultCalendarForNewEvents
                do {
                    try eventStore.save(event, span: .thisEvent)
                } catch let e as NSError {
                    completion?(false, e)
                    return
                }
                completion?(true, nil)
            } else {
                completion?(false, error as NSError?)
            }
        })
    }
    
    //MARK: App functions
    func openAppPage() {
        guard let url = URL(string: GlobalConstants.APPURL) else {
            return //be safe
        }
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url)
        }
    }
    
    func shareApp() {
        
        let text = String(format: "You have been invited to use %@, I love using %@, It's simple and incredible, You should try it here : %@",GlobalConstants.APPNAME ,GlobalConstants.APPNAME,GlobalConstants.APPURL)
        let shareAll = [text]
        let activityViewController = UIActivityViewController(activityItems: shareAll, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view
        self.present(activityViewController, animated: true, completion: nil)
    }
    
    func delayWithSeconds(_ seconds: Double, completion: @escaping () -> ()) {
        DispatchQueue.main.asyncAfter(deadline: .now() + seconds) {
            completion()
        }
    }
}

extension UIControl {
    func addAction(for controlEvents: UIControl.Event = .primaryActionTriggered, action: @escaping () -> ()) {
        let sleeve = ClosureSleeve(attachTo: self, closure: action)
        addTarget(sleeve, action: #selector(ClosureSleeve.invoke), for: controlEvents)
    }
}

extension BaseVC: CLLocationManagerDelegate {
    // Location
    func currentLocation() {
        self.locationManager.requestAlwaysAuthorization()
        
        // For use in foreground
        self.locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
        if locValue.latitude != 0 && locValue.longitude != 0 {
//            AppPrefsManager.sharedInstance.setCurrentLat(obj: locValue.latitude as AnyObject)
//            AppPrefsManager.sharedInstance.setCurrentLong(obj: locValue.longitude as AnyObject)
            locationManager.stopUpdatingLocation()
        } else {
            
        }
        print("locations = \(locValue.latitude) \(locValue.longitude)")
    }
    
    func configuredMailComposeViewController(strMail: String) {
        let mailComposerVC = MFMailComposeViewController()
        mailComposerVC.mailComposeDelegate = self
        mailComposerVC.setToRecipients([strMail])
        mailComposerVC.setSubject("")
        mailComposerVC.setMessageBody("", isHTML: false)
        if MFMailComposeViewController.canSendMail() {
            self.present(mailComposerVC, animated: true, completion: nil)
        }
    }
    
    func removeAllPreference() {
        let domain = Bundle.main.bundleIdentifier!
        UserDefaults.standard.removePersistentDomain(forName: domain)
        UserDefaults.standard.synchronize()
    }
}
