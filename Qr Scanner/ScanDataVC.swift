//
//  ScanDataVC.swift
//  Qr Scanner
//
//  Created by Winsant on 04/07/19.
//  Copyright © 2019 Winsant. All rights reserved.
//

import UIKit
import SQLite3
import GoogleMobileAds

class qrData {
    var id: Int
    var qrData: String?
    var title: String?
    var favorite: Int
    var date : String?
    init(id: Int, qrData: String?, title: String?, fav: Int, date : String){
        self.id = id
        self.qrData = qrData
        self.title = title
        self.favorite = fav
        self.date = date
    }
}

class cellQrData : UITableViewCell{
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblQrCode: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    
}

class ScanDataVC: BaseVC {
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet var viewShare: UIView!
    @IBOutlet weak var lblQrTitle: UILabel!
    @IBOutlet weak var lblQrCode: UILabel!
    @IBOutlet weak var btnShare: UIButton!
    @IBOutlet weak var btnFavorite: UIButton!
    @IBOutlet weak var btnDelete: UIButton!
    @IBOutlet weak var btnLink: UIButton!
    
    @IBOutlet weak var bannerView: GADBannerView!
    var strQrCode : String = String()
    var id : Int = Int()
    var favorite : Int = Int()
    
    var index : Int = Int()
    
    
    var arrQrData = [qrData]()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        btnShare.layer.cornerRadius = 5.0
        btnFavorite.layer.cornerRadius = 5.0
        btnDelete.layer.cornerRadius = 5.0
        btnLink.layer.cornerRadius = 5.0
        
        self.viewShare.frame = self.view.frame
        
        self.tblView.delegate = self
        self.tblView.dataSource = self
        self.tblView.tableFooterView = UIView()
        
        self.setNavigationBar(title: GlobalConstants.APPNAME as NSString, titleImage: nil, leftImage: nil, rightImage: nil, leftTitle: nil, rightTitle: nil, isLeft: false, isRight: false, isLeftMenu: false, isRightMenu: false, bgColor: UIColor.AppColor(), textColor: UIColor.TextColor(), leftClick: { (sender) in
        }) { (sender) in
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.getData()
        self.loadBannerAds(banner: bannerView)
    }
    
    func getData() {
        self.arrQrData.removeAll()
        let queryStatementString = "SELECT * FROM qrData"
        var queryStatement: OpaquePointer? = nil
        
        if sqlite3_prepare_v2(db, queryStatementString, -1, &queryStatement, nil) == SQLITE_OK {
            while(sqlite3_step(queryStatement) == SQLITE_ROW) {
                let id = sqlite3_column_int(queryStatement, 0)
                let quotes = String(cString: sqlite3_column_text(queryStatement, 1))
                let title = String(cString: sqlite3_column_text(queryStatement, 2))
                let fav = sqlite3_column_int(queryStatement, 3)
                let date = String(cString: sqlite3_column_text(queryStatement, 4))
                arrQrData.append(qrData(id: Int(id), qrData: quotes, title: title, fav: Int(fav), date: date))
            }
        } else {
            print("SELECT statement could not be prepared")
        }
        
        if arrQrData.count == 0{
            self.showAlert(title: GlobalConstants.APPNAME, message: "No QR Codes Available")
        }
        
        self.tblView.reloadData()
        
        sqlite3_finalize(queryStatement)
    }
    
    @IBAction func btnCloseClick(_ sender: Any) {
        self.viewShare.removeFromSuperview()
    }
    
    @IBAction func btnShareClick(_ sender: Any) {
        self.shareContent(strContent: strQrCode)
    }
    
    @IBAction func btnFavoriteClick(_ sender: Any) {
        let objDetail = arrQrData[index]
        var favValue: Int = 0
        if objDetail.favorite == 0 {
            favValue = 1
        } else {
            favValue = 0
        }
        var stmtSubCat:OpaquePointer?
        let queryDetailString = String(format: "UPDATE qrData SET favorite = ? WHERE id = %d", objDetail.id)
        
        if sqlite3_prepare(db, queryDetailString, -1, &stmtSubCat, nil) != SQLITE_OK {
            let errmsg = String(cString: sqlite3_errmsg(db)!)
            print("error preparing select: \(errmsg)")
            return
        }
        if sqlite3_bind_int(stmtSubCat, 1, Int32(favValue)) != SQLITE_OK {
            let errmsg = String(cString: sqlite3_errmsg(db)!)
            print("failure binding int: \(errmsg)")
            return
        }
        if sqlite3_step(stmtSubCat) != SQLITE_DONE {
            let errmsg = String(cString: sqlite3_errmsg(db)!)
            print("failure inserting subCat: \(errmsg)")
            return
        } else {
            if objDetail.favorite == 0 {
                objDetail.favorite = 1
                self.showAlert(title: GlobalConstants.APPNAME, message: "Successfully added to favorite...!")
                self.viewShare.removeFromSuperview()
            } else {
                self.showAlert(title: GlobalConstants.APPNAME, message: "Already exist in favorite...!")
                self.viewShare.removeFromSuperview()
            }
        }

    }
    
    @IBAction func btnDelete(_ sender: Any) {
        let objDetail = arrQrData[index]
        
        let id = objDetail.id
        let queryString = "DELETE FROM qrData where id = '\(id)'"
        var queryStatement: OpaquePointer? = nil
        
        if sqlite3_prepare(self.db, queryString, -1, &queryStatement, nil) != SQLITE_OK{
            let errmsg = String(cString: sqlite3_errmsg(self.db)!)
            print("error preparing insert: \(errmsg)")
            return
        }
        while(sqlite3_step(queryStatement) == SQLITE_ROW){
            let id = sqlite3_column_int(queryStatement, 0)
            let quotes = String(cString: sqlite3_column_text(queryStatement, 1))
            let title = String(cString: sqlite3_column_text(queryStatement, 2))
            let fav = sqlite3_column_int(queryStatement, 3)
            let date = String(cString: sqlite3_column_text(queryStatement, 4))
            arrQrData.append(qrData(id: Int(id), qrData: quotes, title: title, fav: Int(fav), date: date))
        }
        
        self.viewShare.removeFromSuperview()
        self.getData()
    }
    
    @IBAction func btnOpenLinkClick(_ sender: Any) {
        guard let url = URL(string: "https://www.google.com") else { return }
        UIApplication.shared.open(url)
    }
}

extension ScanDataVC: UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrQrData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellQrData = Bundle.main.loadNibNamed("Cell", owner: self, options: nil)?[0] as! cellQrData
        let data : qrData = arrQrData[indexPath.row]
        cellQrData.lblTitle?.text = data.title
        cellQrData.lblQrCode?.text = data.qrData
        cellQrData.lblDate.text = data.date
        return cellQrData
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let data : qrData = arrQrData[indexPath.row]
        lblQrTitle.text = data.title
        lblQrCode.text = data.qrData
        strQrCode = data.qrData!
        index = indexPath.row
        self.view.addSubview(viewShare)
    }
}
