//
//  Constant.swift
//  GreekToMe
//
//  Created by Naaz Infotech on 5/14/18.
//  Copyright © 2018 Naaz Infotech. All rights reserved.
//
// swiftlint:disable all

import UIKit
struct GlobalConstants {
    static let APPNAME = "Scan My Code - Best QR Code Scanner"
    static let APPURL = "https://apps.apple.com/in/app/Scan-My-Code - Best-QR-Code-Scanner/id1471602609"
    static let rateAppID = "id1471602609"
    static let adBannerId = "ca-app-pub-5878356407579198/6406823260"
    static let adIntertitialId = "ca-app-pub-5878356407579198/2467578253"
}

enum Storyboard: String {
    case main    = "Main"
    case home    = "Home"
    case login   = "Login"
    case tabBar  = "HomeTabBar"
    
    func storyboard() -> UIStoryboard {
        return UIStoryboard(name: self.rawValue, bundle: Bundle.main)
    }
}

enum Color: String {
    case SliderSelction  = "0080ff"
    case SliderDefault  = "0048b1"
    case SliderTextSelection  = "ffffff"
    case SliderTextDefault  = "7887b5"
    case AppColorCode  = "6E0A5E"
    
    func color() -> UIColor {
        return UIColor.hexStringToUIColor(hex: self.rawValue)
    }
}

enum Identifier: String {
    
    case quotes = "QuotesVC"
    case thought = "ThoughtsVC"
    case info = "InfoVC"
    case bio = "BiographyVC"
    case fav = "FavoriteVC"
    case bioVC = "BioVC"
}



