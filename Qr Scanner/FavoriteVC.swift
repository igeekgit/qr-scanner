//
//  FavoriteVC.swift
//  Qr Scanner
//
//  Created by Winsant on 04/07/19.
//  Copyright © 2019 Winsant. All rights reserved.


import UIKit
import SQLite3
import GoogleMobileAds

class FavoriteVC: BaseVC {
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var bannerView: GADBannerView!
    
    var arrData = [qrData]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tblView.tableFooterView = UIView()
        self.tblView.delegate = self
        self.tblView.dataSource = self
        
        
        
        self.setNavigationBar(title: GlobalConstants.APPNAME as NSString, titleImage: nil, leftImage: nil, rightImage: nil, leftTitle: nil, rightTitle: nil, isLeft: false, isRight: false, isLeftMenu: false, isRightMenu: false, bgColor: UIColor.AppColor(), textColor: UIColor.TextColor(), leftClick: { (sender) in
            
        }) { (sender) in
            
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.getFavData()
        self.loadBannerAds(banner: bannerView)

    }
    
    func getFavData() {
        self.arrData.removeAll()
        
        let queryStatementString = String(format: "SELECT * FROM qrData WHERE favorite = %d", 1)
        var queryStatement: OpaquePointer? = nil
        
        if sqlite3_prepare_v2(db, queryStatementString, -1, &queryStatement, nil) == SQLITE_OK {
            while(sqlite3_step(queryStatement) == SQLITE_ROW) {
                let id = sqlite3_column_int(queryStatement, 0)
                let quotes = String(cString: sqlite3_column_text(queryStatement, 1))
                let title = String(cString: sqlite3_column_text(queryStatement, 2))
                let fav = sqlite3_column_int(queryStatement, 3)
                let date = String(cString: sqlite3_column_text(queryStatement, 4))
                arrData.append(qrData(id: Int(id), qrData: quotes, title: title, fav: Int(fav), date: date))
            }
        } else {
            print("SELECT statement could not be prepared")
        }
        
        if arrData.count == 0{
            self.showAlert(title: GlobalConstants.APPNAME, message: "No records found...!")
            self.navigationController?.popViewController(animated: true)
        }
        
        self.tblView.reloadData()
        sqlite3_finalize(queryStatement)
    }
    
}


extension FavoriteVC : UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellFav = Bundle.main.loadNibNamed("Cell", owner: self, options: nil)?[0] as! cellQrData
        let data : qrData = arrData[indexPath.row]
        cellFav.lblTitle?.text = data.title
        cellFav.lblQrCode?.text = data.qrData
        cellFav.lblDate.text = data.date
        return cellFav
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let removeFav = UITableViewRowAction(style: UITableViewRowAction.Style.default, title: "Remove", handler:{action, indexpath in
            self.removeFavData(index: indexpath.row)
        });
        removeFav.backgroundColor = UIColor.AppColor()
        return [removeFav];
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    func removeFavData(index : Int) {
        let objDetail = arrData[index]
        var favValue: Int = 0
        if objDetail.favorite == 0 {
            favValue = 1
        } else {
            favValue = 0
        }
        var stmtSubCat:OpaquePointer?
        let queryDetailString = String(format: "UPDATE qrData SET favorite = ? WHERE id = %d", objDetail.id)
        
        if sqlite3_prepare(db, queryDetailString, -1, &stmtSubCat, nil) != SQLITE_OK {
            let errmsg = String(cString: sqlite3_errmsg(db)!)
            print("error preparing select: \(errmsg)")
            return
        }
        if sqlite3_bind_int(stmtSubCat, 1, Int32(favValue)) != SQLITE_OK {
            let errmsg = String(cString: sqlite3_errmsg(db)!)
            print("failure binding int: \(errmsg)")
            return
        }
        if sqlite3_step(stmtSubCat) != SQLITE_DONE {
            let errmsg = String(cString: sqlite3_errmsg(db)!)
            print("failure inserting subCat: \(errmsg)")
            return
        } else {
            self.getFavData()
        }
    }
    
    
}
