//
//  ViewController.swift
//  Qr Scanner
//
//  Created by Winsant on 04/07/19.
//  Copyright © 2019 Winsant. All rights reserved.
//

import UIKit
import AVFoundation
import GoogleMobileAds

class ViewController: BaseVC, QRCodeReaderViewControllerDelegate {
    
    @IBOutlet weak var txtTitle: UITextField!
    @IBOutlet var viewTitle: UIView!
    @IBOutlet weak var btndone: UIButton!
    @IBOutlet weak var btnFlash: UIButton!
    var strCode : String = String()
    @IBOutlet weak var bannerView: GADBannerView!
    @IBOutlet weak var viewScanner: QRCodeReaderView!{
        didSet {
            viewScanner.setupComponents(showCancelButton: false, showSwitchCameraButton: false, showTorchButton: false, showOverlayView: true, reader: reader)
        }
    }
    
    lazy var reader: QRCodeReader = QRCodeReader()
    lazy var readerVC: QRCodeReaderViewController = {
        let builder = QRCodeReaderViewControllerBuilder {
            $0.reader                  = QRCodeReader(metadataObjectTypes: [.qr], captureDevicePosition: .back)
            $0.showTorchButton         = true
            $0.preferredStatusBarStyle = .lightContent
            $0.reader.stopScanningWhenCodeIsFound = false
        }
        return QRCodeReaderViewController(builder: builder)
    }()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tabBarController?.tabBar.isTranslucent = true
        
        self.txtTitle.layer.borderColor = UIColor.TextColor().cgColor
        self.txtTitle.layer.borderWidth = 1.0
        self.txtTitle.layer.cornerRadius = 5.0
        
        self.btndone.layer.cornerRadius = 5.0
        
        self.viewTitle.frame = self.view.frame
        
        self.view.backgroundColor = UIColor.hexStringToUIColor(hex: "#c1f4ff")
        self.setNavigationBar(title: GlobalConstants.APPNAME as NSString, titleImage: nil, leftImage: nil, rightImage: nil, leftTitle: nil, rightTitle: nil, isLeft: false, isRight: false, isLeftMenu: false, isRightMenu: false, bgColor: UIColor.AppColor(), textColor: UIColor.TextColor(), leftClick: { (sender) in
        }) { (sender) in
            
        }
        
        guard checkScanPermissions(), !reader.isRunning else { return }
        reader.didFindCode = { result in
            self.strCode = result.value
            self.view.addSubview(self.viewTitle)
            self.reader.startScanning()
            
            print("Completion with result: \(result.value) of type \(result.metadataType)")
//            self.presentAlertWithTitle(presentStyle: .alert, title: "Result", message: result.value,    options: "Ok", completion: { (index) in
//                self.strCode = result.value
//                self.view.addSubview(self.viewTitle)
//                self.reader.startScanning()
//            })
        }
        
        self.reader.startScanning()
        
    }
    
    
    
    
    
    
    @IBAction func btnFlashClick(_ sender: Any) {
        
        let device = AVCaptureDevice.default(for: AVMediaType.video)
        if (device!.hasTorch) {
            do {
                try device!.lockForConfiguration()
                if (device?.torchMode == AVCaptureDevice.TorchMode.on) {
                    device!.torchMode = AVCaptureDevice.TorchMode.off
                } else {
                    do {
                        try device!.setTorchModeOn(level: 1.0)
                    } catch {
                        print(error)
                    }
                }
                device!.unlockForConfiguration()
            } catch {
                print(error)
            }
        }
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.viewTitle.removeFromSuperview()
        self.loadBannerAds(banner: bannerView)
    }
    
    func reader(_ reader: QRCodeReaderViewController, didScanResult result: QRCodeReaderResult) {
        reader.stopScanning()
        dismiss(animated: true) { [weak self] in
            let alert = UIAlertController(
                title: "Qr Scanner Result",
                message: String (format:"%@ (of type %@)", result.value, result.metadataType),
                preferredStyle: .alert
            )
            alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
            self?.present(alert, animated: true, completion: nil)
        }
    }
    
    func readerDidCancel(_ reader: QRCodeReaderViewController) {
        reader.stopScanning()
        dismiss(animated: true, completion: nil)
    }
    @IBAction func btnDoneClick(_ sender: Any) {
        if self.txtTitle.text?.count == 0
        {
            
        }
        else
        {
            self.insertData(qrCode: strCode as NSString, title: txtTitle!.text! as NSString)
            self.txtTitle.text = ""
            self.viewTitle.removeFromSuperview()
        }
    }
    
    private func checkScanPermissions() -> Bool {
        do {
            return try QRCodeReader.supportsMetadataObjectTypes()
        } catch let error as NSError {
            let alert: UIAlertController
            switch error.code {
            case -11852:
                alert = UIAlertController(title: "Error", message: "This app is not authorized to use Back Camera.", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Setting", style: .default, handler: { (_) in
                    DispatchQueue.main.async {
                        if let settingsURL = URL(string: UIApplication.openSettingsURLString) {
                            UIApplication.shared.openURL(settingsURL)
                        }
                    }
                }))
                
                alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
            default:
                alert = UIAlertController(title: "Error", message: "Reader not supported by the current device", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
            }
            present(alert, animated: true, completion: nil)
            return false
        }
    }
}

extension UIViewController {
    
    func presentAlertWithTitle(presentStyle: UIAlertController.Style ,title: String, message: String, options: String..., completion: @escaping (Int) -> Void) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: presentStyle)
        for (index, option) in options.enumerated() {
            alertController.addAction(UIAlertAction.init(title: option, style: .default, handler: { (action) in
                completion(index)
            }))
        }
        self.present(alertController, animated: true, completion: nil)
        
    }
}

